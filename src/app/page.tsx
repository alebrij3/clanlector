import Image from "next/image";
import {reads} from "../../store/db";
import React from "react";
import {currentBook} from "../../store/db";

const current = reads.find(read => read.id === currentBook);
const past = reads.filter(read => read.id !== currentBook).reverse();

export default function Home() {
    return (
        <main className="min-h-screen">
            <CurrentRead title={current?.title ?? ''} author={current?.author ?? ''} bookCover={current?.bookCover ?? ''}/>
            <div className="flex justify-center">
                <div className="flex md:p-24 p-6 justify-center max-w-screen-lg">
                    <div className="grid md:grid-cols-4 grid-cols-2 gap-8 md:gap-14">
                        {past.map((read: IBook) => (
                            <Book key={read.id} title={read.title} author={read.author} bookCover={read.bookCover}/>))}
                    </div>
                </div>
            </div>
        </main>

    );
}

const Book: React.FC<IBook> = ({title, author, bookCover}) => {
    return (
        <div>
            <img className="mb-2 rounded-md" src={bookCover} alt=""/>
            <p className="text-base md:text-lg leading-tight md:leading-tight mb-1">{title}</p>
            <p className="text-sm leading:tight md:leading-tight text-slate-500">{author}</p>
        </div>
    )
}

const CurrentRead: React.FC<IBook> = ({title, author, bookCover}) => {
    return (
        <div className="bg-amber-100 p-12 bg-gradient-to-r from-teal-800 to-emerald-900">
            <h2 className="text-2xl text-center text-slate-50">Estamos leyendo:</h2>
            <div className="md:flex flex-columns items-center justify-center">
                <img className="md:max-w-64 my-8 shadow-md w-full md:mr-4" src={bookCover} alt=""/>
                <div>
                    <p className="text-4xl text-slate-50">{title}</p>
                    <p className="text-2xl text-slate-50">{author}</p>
                </div>
            </div>
        </div>
    )
}

interface IBook {
    id?: number;
    author: string;
    title: string;
    bookCover: string;
}
