export const currentBook = 52;
const bookCoverBucket = 'https://storage.googleapis.com/clanlector.com/portadas';
export const reads = [
  {
    id: 2,
    title: 'Flores en el ático',
    author: 'V. C. Andrews',
    bookCover: `${bookCoverBucket}/flores-en-el-atico.jpeg`,
    link: '',
    dateStart: '2021-01-01'
  },
  {
    id: 3,
    title: 'Historia sobre un corazón roto... y tal vez un par de colmillos',
    author: 'M. B. Brozon',
    bookCover: `${bookCoverBucket}/historia-sobre-un-corazon-roto.jpg`,
    dateStart: '2021-02-01',
    link: '',
  },
  {
    id: 4,
    title: 'Mágico, sombrío, impenetrable',
    author: 'Joyce Carol Oates',
    bookCover: `${bookCoverBucket}/magico-sombrio-impenetrable.jpeg`,
    dateStart: '2021-03-01'
  },
  {
    id: 5,
    title: 'Mundo Umbrío II - La venganza',
    author: 'Jaime Alfonso Sandoval',
    bookCover: `${bookCoverBucket}/mundo-umbrio-ii.jpeg`,
    dateStart: '2021-04-01'
  },
  {
    id: 6,
    title: 'Mundo Umbrío III - La traición',
    author: 'Jaime Alfonso Sandoval',
    bookCover: `${bookCoverBucket}/mundo-umbrio-iii.jpeg`,
    dateStart: '2021-05-01'
  },
  {
    id: 7,
    title: 'El club de la salamandra',
    author: 'Jaime Alfonso Sandoval',
    bookCover: `${bookCoverBucket}/el-club-de-la-salamandra.jpg`,
    dateStart: '2021-06-01'
  },
  {
    id: 8,
    title: 'Campamento miedo',
    author: 'Jaime Alfonso Sandoval',
    bookCover: `${bookCoverBucket}/campamento-miedo.jpeg`,
    dateStart: '2021-07-01'
  },
  {
    id: 9,
    title: 'Melocotón loco',
    author: 'Megan Maxwell',
    bookCover: `${bookCoverBucket}/melocoton-loco.jpeg`,
    dateStart: '2021-08-01'
  },
  {
    id: 10,
    title: 'Hacerse el muerto',
    author: 'Andrés Neuman',
    bookCover: `${bookCoverBucket}/hacerse-el-muerto.jpeg`,
    dateStart: '2021-09-01'
  },
  {
    id: 11,
    title: 'Yo, la peor',
    author: 'Mónica Lavín',
    bookCover: `${bookCoverBucket}/yo-la-peor.jpg`,
    dateStart: '2021-10-01'
  },
  {
    id: 12,
    title: 'Un monstruo viene a verme',
    author: 'Patrick Ness',
    bookCover: `${bookCoverBucket}/un-monstruo-viene-a-verme.jpg`,
    dateStart: '2021-11-01'
  },
  {
    id: 13,
    title: '¿Sueñan los androides con ovejas eléctricas?',
    author: 'Philip K. Dick',
    bookCover: `${bookCoverBucket}/suenan-los-androides-con-ovejas-electricas.jpg`,
    dateStart: '2021-12-01'
  },
  {
    id: 14,
    title: 'Ciudad Miedo',
    author: 'Jaime Alfonso Sandoval',
    bookCover: `${bookCoverBucket}/ciudad-miedo.jpg`,
    dateStart: '2022-01-01'
  },
  {
    id: 15,
    title: 'Siete esqueletos decapitados',
    author: 'Antonio Malpica',
    bookCover: `${bookCoverBucket}/siete-esqueletos-decapitados.jpeg`,
    synopsis:
      'Tres niños han desaparecido en la ciudad de México. La única pista que tiene la policía es que se trata de casos relacionados, ya que las familias, al cabo de un par de días del secuestro, reciben un macabro obsequio: un costal con los huesos y la ropa ensangrentada de los chicos. Nadie sospecha que Sergio Mendhoza, un adolescente de 13 años, es el único capaz de resolver este misterio. Una escalofriante obra situada en el México contemporáneo, que escenifica con singular aplomo la lucha entre el bien y el mal.',
    dateStart: '2022-02-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [
      {
        link: 'https://www.youtube.com/watch?v=vqrxAl2Oi9k',
        thumbnail: '/img/video-thumbnails/vqrxAl2Oi9k.webp',
        title: 'LC 7 ESQUELETOS DECAPITADOS [Primer live]',
        description: '',
      },
    ],
  },
  {
    id: 16,
    title: 'La ciudad de las esfinges',
    author: 'Jaime Alfonso Sandoval',
    bookCover: `${bookCoverBucket}/la-ciudad-de-las-esfinges.jpg`,
    dateStart: '2020-07-01'
  },
  {
    id: 17,
    title: 'Mexicoland',
    author: 'Jaime Alfonso Sandoval',
    bookCover: `${bookCoverBucket}/mexicoland.jpg`,
    dateStart: '2020-09-01'
  },
  {
    id: 18,
    title: 'Mundo Umbrío I',
    author: 'Jaime Alfonso Sandoval',
    bookCover: `${bookCoverBucket}/mundo-umbrio-i.jpeg`,
    dateStart: '2020-10-01'
  },
  {
    id: 19,
    title: 'Casas vacías',
    author: 'Brenda Navarro',
    bookCover: `${bookCoverBucket}/casas-vacias.jpeg`,
    dateStart: '2020-11-01'
  },
  {
    id: 20,
    title: 'Las cosas que perdimos en el fuego',
    author: 'Mariana Enríquez',
    bookCover: `${bookCoverBucket}/las-cosas-que-perdimos-en-el-fuego.jpeg`,
    dateStart: '2020-11-01'
  },
  {
    id: 22,
    title: 'Doce sustos y un perico',
    author: 'Jaime Alfonso Sandoval',
    bookCover: `${bookCoverBucket}/doce-sustos-y-un-perico.jpg`,
    dateStart: '2020-12-01'
  },
  {
    id: 23,
    title: 'La sombra del viento',
    author: 'Carlos Ruiz Zafón',
    bookCover: `${bookCoverBucket}/la-sombra-del-viento.jpeg`,
    synopsis:
      'Un amanecer de 1945 un muchacho es conducido por su padre a un misterioso lugar oculto en el corazón de la ciudad vieja: El Cementerio de los Libros Olvidados. Allí, Daniel Sempere encuentra un libro maldito que cambiará el rumbo de su vida y le arrastrará a un laberinto de intrigas y secretos enterrados en el alma oscura de la ciudad. La sombra del viento es un misterio literario ambientado en la Barcelona de la primera mitad del siglo XX, desde los últimos esplendores del Modernismo a las tinieblas de la posguerra.',
    dateStart: '2022-04-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 24,
    title: 'Heartstopper (vol. 1-4)',
    author: 'Alice Oseman',
    bookCover: `${bookCoverBucket}/heartstopper.jpeg`,
    synopsis:
      'Charlie y Nick van al mismo colegio, aunque nunca se habían cruzado hasta el día en que los hacen sentarse juntos en su grupo de estudio. Muy pronto se vuelven amigos y más pronto aún Charlie comienza a sentir cosas por Nick… aunque sabe que es un imposible. Pero el amor obra de formas inesperadas, y Nick está más interesado en Charlie de lo que ninguno de los dos puede llegar a creer.',
    dateStart: '2022-05-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 25,
    title: 'La última casa en la montaña',
    author: 'Xavier M. Sotelo',
    bookCover: `${bookCoverBucket}/la-ultima-casa-en-la-montana.jpeg`,
    synopsis:
      'Un terrible accidente automovilístico cambia para siempre la vida de los pequeños Max y Poncho, así como la del oficial Esteban Rey. Para los niños, perder a sus padres tal vez no sea la mayor desgracia que enfrentarán, pues se verán obligados a vivir con una pariente que jamás han conocido: la tía Carmen, cuyas ideas y convicciones son más horrendas que los fantasmas de la casa que habitarán.\nEsteban Rey, mientras tanto, está demasiado atormentado y cegado por su propia pérdida, y hará caso omiso de las advertencias que recibe. No detendrá su investigación hasta descubrir las verdaderas causas del accidente, y se irá convirtiendo en el mejor aliado de los niños en su intento por sobrevivir la pesadilla de la última casa en la montaña.',
    dateStart: '2022-07-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 26,
    title: 'Watchmen',
    author: 'Alan Moore',
    bookCover: `${bookCoverBucket}/watchmen.jpg`,
    synopsis:
      'Watchmen se desarrolla en una realidad alternativa que refleja fielmente el mundo contemporáneo de la década de 1980. La principal diferencia es la presencia de superhéroes. El punto de divergencia se produce en el año 1938 y muestra cómo su existencia ha afectado o alterado de manera dramática los resultados de la guerra de Vietnam y la presidencia de Richard Nixon.',
    dateStart: '2022-08-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 27,
    title: 'El nombre de la rosa',
    author: 'Umberto Eco',
    bookCover: `${bookCoverBucket}/el-nombre-de-la-rosa.jpg`,
    synopsis:
      'El nombre de la rosa narra las investigaciones detectivescas que realiza el fraile franciscano Guillermo de Baskerville para esclarecer los crímenes cometidos en una abadía benedictina en el año 1327. Le ayudará en su labor el novicio Adso, un joven que se enfrenta por primera vez a las realidades de la vida situadas más allá de las puertas del convento.',
    dateStart: '2022-09-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 28,
    title: 'Los cuentos de la nana Buba',
    author: 'Jaime Alfonso Sandoval',
    bookCover: `${bookCoverBucket}/mundo-umbrio-iii.jpeg`,
    synopsis:
      'Cuentos de cuna para la sanguaza: Esqueletos bailarines, pequeños umbríos hambrientos, hechicería y más en estos cuentos de las nuevas ediciones de la saga Mundo Umbrío.',
    dateStart: '2022-08-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 29,
    title: 'Mundo Umbrío IV Guerra de guerras',
    author: 'Jaime Alfonso Sandoval',
    bookCover: `${bookCoverBucket}/mundo-umbrio-iv.jpg`,
    synopsis:
      'Guerreros, magos oscuros, monstruos de la infratierra, cadáveres revividos, todos entran en la batalla. Lina debe prepararse para pelear, mientras investiga dónde quedó Gismundus e intenta evadir una escalofriante profecía que apunta hacia ella.',
    dateStart: '2022-08-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 30,
    title: 'Nick y Charlie',
    author: 'Alice Oseman',
    bookCover: `${bookCoverBucket}/nick-y-charlie.webp`,
    synopsis:
      'Todo el mundo sabe que Nick y Charlie son la pareja perfecta y que son inseparables, pero ahora Nick se irá a la universidad y Charlie se quedará atrás, todos se preguntan si seguirán juntos. ¡Qué pregunta tan tonta! ¡Por favor! ¡Si son Nick y Charlie! Pero cuando la despedida se acerque, los dos jóvenes empezarán a cuestionarse  si su amor es lo suficientemente fuerte para sobrevivir a la distancia o si solo están retrasando lo inevitable...Porque todo el mundo sabe que el primer amor nunca es para siempre ¿o sí lo es?',
    dateStart: '2022-05-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 31,
    title: 'Al final las palabras',
    author: 'Antonio Malpica',
    bookCover: `${bookCoverBucket}/al-final-las-palabras.jpg`,
    synopsis:
      '"Lo que importa es la historia dentro de la Historia", asegura uno de los personajes de esta novela histórica con tintes románticos que Toño Malpica desarrolla en tres diferentes épocas y que sobrevive gracias a un manuscrito que ha pasado de mano en mano, manuscrito que narra la amistad y las andanzas de un grupo de chicos que está dejando atrás la infancia en la Ciudad de México de los inicios del siglo XX, la vida pintoresca de la capital y el descubrimiento del amor que experimentan el Pegote y Ofelia. Esta historia de amor se ve frustrada por la partida de Ofelia, quien se muda con su familia a París, y quien después de mucho tiempo, a mediados de los setenta, decide contratar a Jesús Rivera para que indague el paradero de su antiguo novio. Así, la historia sobre el Pegote, un chico que es tan bueno para los albures y la baraja como lo es para los trancazos y las patadas, se va completando a través del manuscrito recuperado por Rivera y de los testimonios que le brindan distintos personajes que lo conocieron. Finalmente, en los años recientes, el manuscrito y la investigación de Rivera llegan a las manos de José Álvez, un escritor que descubre en la historia de Ofelia y Pegote una razón para volver la mirada hacia atrás, a su propia historia.',
    dateStart: '2023-02-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 32,
    title: 'La canción del lobo',
    author: 'T. J. Klune',
    bookCover: `${bookCoverBucket}/la-cancion-del-lobo.jpeg`,
    synopsis:
        'Ox era un niño cuando su padre le enseñó que nunca sería nadie.Y lo creyó por mucho tiempo.Hasta que se encontró a Joe al final de un camino. Un chico extraño y explosivo, dispuesto a brindarle todo: desde su amistad y su familia, hasta su mayor secreto: uno que teñirá la vida de Ox de Alfas, Betas y Omegas.Sin embargo, cuando la muerte golpea a las puertas de Green Creek, Joe parte detrás de un monstruo, cegado por la furia y la venganza. Y Ox deberá demostrarsu verdadero valor para proteger a quienes ama.Cuando vuelvan a encontrarse, ¿serán capaces de resistir a la canción que aúlla con fuerza entre los dos?',
    dateStart: '2023-03-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 33,
    title: 'El monje',
    author: 'Matthew Lewis',
    bookCover: `${bookCoverBucket}/el-monje.jpeg`,
    synopsis:
        'Matthew G. Lewis. (1775 - 1818). Escritor, dramaturgo y político británico conocido por Monk Lewis a raíz de su primera obra, El Monje (1796), donde denunciaba la Inquisición española y que le hizo popular. El Monje, de buena acogida entre la mayoría de la población, fue muy criticado por obsceno entre los intelectuales británicos, lo que obligó al autor a dulcificar la segunda edición de 1798, publicada cuando ya era miembro del Parlamento. Es una novela gótica donde se ironiza sobre la hipocresía religiosa. Lord Byron y el Marqués de Sade dieron su visto bueno a la novela en sus correspondientes escritos. El Monje fue reivindicada por André Breton y Antonin Artaud como la mejor novela gótica y uno de los mayores logros del Romanticismo.',
    dateStart: '2023-04-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 34,
    title: 'Arcanum XIII',
    author: 'Joaquín Ochoa González',
    bookCover: `${bookCoverBucket}/arcanum-xiii.jpg`,
    synopsis:
        'Arcanum XIII es una antología de cuentos breves ilustrados, inspirados en varios temas literarios, como: terror, misterio, suspenso, intriga, ciencia ficción, filosofía, drama, misticismo, psicología, cuento infantil, etc. Un libro apto para quienes gustan de lecturas rápidas y entretenidas, pero también amplio para aquellos que desean profundizar en los temas que se abordan.',
    dateStart: '2023-05-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 35,
    title: 'La culpa es de los tlaxcaltecas',
    author: 'Elena Garro',
    bookCover: `${bookCoverBucket}/la-culpa-es-de-los-tlaxcaltecas.jpg`,
    synopsis:
      '',
    dateStart: '2023-11-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 36,
    title: 'Los dragones del Edén',
    author: 'Carl Sagan',
    bookCover: `${bookCoverBucket}/los-dragones-del-eden.jpg`,
    synopsis:
      '',
    dateStart: '2023-09-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 37,
    title: 'Los inmortales',
    author: 'Chloe Benjamin',
    bookCover: `${bookCoverBucket}/los-inmortales.jpg`,
    synopsis:
      '',
    dateStart: '2023-10-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 38,
    title: 'Tiempos canallas',
    author: 'Jaime Alfonso Sandoval',
    bookCover: `${bookCoverBucket}/tiempos-canallas.jpg`,
    synopsis:
      '',
    dateStart: '2024-02-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 39,
    title: 'Ensayo sobre la ceguera',
    author: 'José Saramago',
    bookCover: `${bookCoverBucket}/ensayo-sobre-la-ceguera.jpg`,
    synopsis:
        '',
    dateStart: '2024-03-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 40,
    title: 'La vida invisible de Addie LaRue',
    author: 'V. E. Schwab',
    bookCover: `${bookCoverBucket}/la-vida-invisible-de-addie-larue.jpg`,
    synopsis:
        '',
    dateStart: '2024-04-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 41,
    title: 'La invención de Morel',
    author: 'Adolfo Bioy Casares',
    bookCover: `${bookCoverBucket}/la-invencion-de-morel.jpg`,
    synopsis:
        '',
    dateStart: '2024-05-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 42,
    title: 'Cuentos justos para tiempos injustos',
    author: 'Jaime Alfonso Sandoval',
    bookCover: `${bookCoverBucket}/cuentos-justos-para-tiempos-injustos.jpg`,
    synopsis:
        '',
    dateStart: '2024-06-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 43,
    title: 'Siete esqueletos decapitados',
    author: 'Antonio Malpica',
    bookCover: `${bookCoverBucket}/siete-esqueletos-decapitados.jpeg`,
    synopsis:
        '',
    dateStart: '2024-06-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 44,
    title: 'Nocturno Belfegor',
    author: 'Antonio Malpica',
    bookCover: `${bookCoverBucket}/nocturno-belfegor.jpg`,
    synopsis:
        '',
    dateStart: '2024-06-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 45,
    title: 'El llamado de la estirpe',
    author: 'Antonio Malpica',
    bookCover: `${bookCoverBucket}/el-llamado-de-la-estirpe.jpg`,
    synopsis:
        '',
    dateStart: '2024-06-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 46,
    title: 'El destino y la espada',
    author: 'Antonio Malpica',
    bookCover: `${bookCoverBucket}/el-destino-y-la-espada.png`,
    synopsis:
        '',
    dateStart: '2024-06-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 47,
    title: 'Principio y fin',
    author: 'Antonio Malpica',
    bookCover: `${bookCoverBucket}/principio-y-fin.jpg`,
    synopsis:
        '',
    dateStart: '2024-06-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 48,
    title: 'Dientes rojos',
    author: 'Jesús Cañadas',
    bookCover: `${bookCoverBucket}/dientes-rojos.jpg`,
    synopsis:
        '',
    dateStart: '2024-07-01',
    readers: ['mau', 'yuli', 'ale', 'sol', 'vanne'],
    lives: [],
  },
  {
    id: 49,
    title: 'Como caracol',
    author: 'Alaide Ventura Medina',
    bookCover: `${bookCoverBucket}/como-caracol.JPG`,
    synopsis:
        '',
    dateStart: '2024-08-06',
    readers: ['mau', 'yuli', 'ale', 'vanne'],
    lives: [],
  },
  {
    id: 50,
    title: 'Hueles a peligro',
    author: 'Fabiana Peralta',
    bookCover: `${bookCoverBucket}/hueles-a-peligro.jpg`,
    synopsis:
        '',
    dateStart: '2024-09-06',
    readers: ['mau', 'yuli', 'ale', 'vanne'],
    lives: [],
  },
  {
    id: 51,
    title: 'El juego del protagonista sin nombre',
    author: 'Antonio Malpica',
    bookCover: `${bookCoverBucket}/el-juego-del-protagonista-sin-nombre.JPG`,
    synopsis:
        '',
    dateStart: '2024-10-01',
    readers: ['mau', 'yuli', 'ale', 'vanne'],
    lives: [],
  },
  {
    id: 52,
    title: 'La sociedad de los dragones del té',
    author: 'Katie O\'Neil',
    bookCover: `${bookCoverBucket}/la-sociedad-de-los-dragones-del-te.jpg`,
    synopsis:
        '',
    dateStart: '2024-10-01',
    readers: ['mau', 'yuli', 'ale', 'vanne'],
    lives: [],
  },
];
